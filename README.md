# Pokemon Trainer

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

**Web application** with **Angular** framework.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.0.0.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)

## Background

This application is an online pokemon trainer. The users can enter their loginname and select their favourite pokemons from the API. On the trainer page the users can see the favourite pokemons that have been collected.

The application is deployed via Heroku and can be accessed at  https://pokemon-trainer-assignment-06.herokuapp.com/login

## Install

- install Node.js
- clone repository
- run ```ng serve``` in the root folder

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Usage

To start the application locally, run ```npm i, ng s`` 

The application is deployed via Heroku and can be accessed at https://pokemon-trainer-assignment-06.herokuapp.com/login

## Maintainers

- [Laszlo Laki](https://gitlab.com/lakilukelaszlo)
- [Adam Olah](https://gitlab.com/adam-olah93)
- [Krisztina Pelei](https://gitlab.com/kokriszti)
