import { Component, OnInit } from '@angular/core';
import {PokemonCatalogueService} from "../../services/pokemon-catalogue.service";
import {Pokemon} from "../../models/pokemon.model";

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.scss']
})
export class PokemonCataloguePage implements OnInit {

  get pokemons(): Pokemon[] {
    return this.pokemonCatalogueService.pokemons;
  }

  get loadnig(): boolean {
    return this.pokemonCatalogueService.loading;
  }

  get error(): String {
    return this.pokemonCatalogueService.error;
  }

  constructor(private readonly pokemonCatalogueService: PokemonCatalogueService) { }

  ngOnInit(): void {
    this.pokemonCatalogueService.findAllPokemons();
  }

}
