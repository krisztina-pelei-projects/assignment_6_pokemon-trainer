import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, switchMap, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

const { apiUsers, apiKey } = environment;

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private readonly http: HttpClient) {}

  //Login method
  public login(username: string): Observable<User> {
    return this.checkUser(username).pipe(
      switchMap((user: User | undefined) => {
        if (user === undefined) {
          // user does not exist
          return this.createUser(username);
        }
        return of(user);
      })
    );
  }

  //Check if user exists
  private checkUser(username: string): Observable<User | undefined> {
    return this.http
      .get<User[]>(`${apiUsers}?username=${username}`)
      .pipe(map((response: User[]) => response.pop()));
  }

  //This method creates a new User object
  private createUser(username: string): Observable<User> {
    const user = { username, favourites: [] }; //User object to be posted

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': apiKey,
    }); //Headers declaration

    return this.http.post<User>(apiUsers, user, {
      headers,
    }); //POST method
  }
}
